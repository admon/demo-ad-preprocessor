import json
import tornado
import random
import logging
import datetime
import kfserving
import numpy as np
import pandas as pd
from typing import *

logging.basicConfig(level=kfserving.constants.KFSERVING_LOGLEVEL)


def preprocess(instance):
    payload = json.loads(instance["payload"])
    # At this point you have access to groupBy column values and interval information, to tune preprocessing
    # instance["host"] # example grouping column, only available if you made a groupBy
    # instance["intervalStart"] # always available
    # instance["intervalEnd"] # always available
    df = pd.DataFrame.from_dict(payload)
    return preprocess_dataframe(df)

# example preprocessing. Model requires feature vector for single prediction.
def preprocess_dataframe(payload_df) -> List[float]:
    clean_df = reformat_and_clean_df(payload_df)
    return [clean_df["value"].min(), clean_df["value"].mean(), clean_df["value"].max()]

def reformat_and_clean_df(df):
    df = df.replace("null", np.nan)
    df = df.dropna()
    df = df.astype({"value": "double", "time": "double"})   # cast json string values to numbers
    df = df.astype({"time": "int"})
    return df


class PreprocessTransformer(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.predictor_host = predictor_host
        logging.info(f"MODEL NAME {name}")
        logging.info(f"PREDICTOR URL {self.predictor_host}")

    def preprocess(self, inputs: Dict[str, List[Dict]]) -> Dict:
        preprocessed_data = list(map(preprocess, inputs["instances"]))
        logging.info("Preprocess-Transformation successfull, calling model:predict")
        return { "instances": preprocessed_data }

    # TODO: remove override of this function, call to url should be enough
    async def predict(self, request: Dict) -> Dict:
        response = await self._http_client.fetch(
            'http://ml.cern.ch/v1/models/collectd-memory-model:predict',
            headers={
                'Host': 'collectd-memory-model-predictor-default.admon-dev.svc.cluster.local'
            },
            method='POST',
            request_timeout=self.timeout,
            body=json.dumps(request)
        )
        if response.code != 200:
            raise tornado.web.HTTPError(
                status_code=response.code,
                reason=response.body)
        return json.loads(response.body)
